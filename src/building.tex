\section{Core concepts}
Building resource adaptive systems~\cite{hughes2016building} requires the availability of adaptations
corresponding to different resource availability profiles.
The adaptations required may either be pre-computed by developers or may be
computed at runtime, based on \sofs. We use test annotations to
capture the relaxation of a specification, to drive the production of
a smaller program that still satisfies part of a system's
specification.  Because it has less code, the adaptation can be
expected to use fewer resources, especially as resources are often
associated with functionalities of a system.

\subsection{Test  Annotations}

We propose test annotation as one of the ways
to capture adaptive requirements~\cite{cheng2009softwareengineering}.
The idea is that a formal specification of different aspects of a
specification, and how those aspects relate, is difficult to produce,
and seldom available for existing systems.  However, most systems,
especially critical systems, have test suites, and the tests in such
suites are often possible to group by \emph{what they test}.
In a complete instantiation of the approach proposed by this paper,
annotations would likely be multi-dimensional, specifying priority of
tests, aspects of behavior covered by tests (as in the example above),
and the resources used by the tests (which relate to the resources
used by different functionalities of the system).  However, the
technique works so long as the labeling of tests allows us to select
some tests that are not required to pass.  In this paper, we assume a
simple one-dimensional labeling scheme, mostly based on priority.
Tests labeled $0$ are critical tests -- if these fail, the system is
not useful.  Higher label values indicate a higher degree of
sacrificability of the specification(s) embodied in the labeled test.
We primarily assume labeling is applied to tests, but it could also be
applied to system invariants that apply across tests, or to individual
assertions or checks inside a single test.  Such a system clearly has
a finer granularity, but also imposes more burden on a user.  Labeling
some sets of tests as 1) related and 2) potentially sacrificable in
exchange for resource adaptation seems to be a relatively light burden
for a user.  In the long run, we would like to automatically produce
approximate annotations, perhaps based on recently proposed schemes
for naming tests~\cite{frasernaming}.

\subsection{(1-)Minimal Program}
Zeller and Hildebrandt.~\cite{zeller2002simplifying} define various notions of minimality for
test-case reduction.  The most important such notion in
delta-debugging is the idea of a \emph{1-minimal} test (for us,
program).  In normal delta-debugging, a test is 1-minimal when no
single component of the test can be removed without the test passing
(recall that delta-debugging's goal is to produce small failing tests
from large failing tests).  A program is 1-minimal if no \emph{single} candidate
element of a program can be removed without the program
failing some required test.  That is, given a program $P$ and a test
suite $T' \subset T$ (where $T$ is the full test suite for the
program), $P$ is 1-minimal iff $\forall t \in T' . pass(t,P)$ and
$\neg \exists P'$ such that $\forall t \in T' . pass(t,P')$ and $P
\Rightarrow P'$, where $\Rightarrow$ represents a single step of
reduction (in our case, removing certain parts of the syntax tree
for $P$).  1-minimality is obviously a local minimality; there may
exist some smaller subset of $P$ that passes $T'$, but would require
applying multiple removals at once to produce a consistent program.
Searching for such a program is prohibitively expensive, in that it
would essentially require enumerating all valid subsets of a program.

\begin{comment}
\subsubsection{Applying mutations for generating adapted versions}
An adapted version of a program is still close enough to the original program,
except for certain small perturbations in the functionality. The easiest way
to generate such changes is using mutation operators from mutation analysis.
We use the \emph{statement deletion} mutation operator to seed sufficient
perturbations in the program so that variations in resource usage are
produced.
\end{comment}
\begin{comment}
% Indeed, there is a reason for choosing statement deletion mutations unlike say
% Picireny which goes deeper than program statements. The reason is that...

% Rahul: The stuff about subsumption of operators does not actually make much sense
% in this context. Subsumption does not mean that it can produce larger faults
% rather it produces more subtle faults i.e smaller faults.
% so the following is incorrect.
When HDD is traditionally applied on programs, it relies on tree structure of the program like AST, CFG or ECFG etc. For example, picireny relied on ECFG of programs. Its reduction run even deeper then program statements, it even reduces expressions. Our approach does not go beyond SDL because (1) SDL operator was found to be subsuming other mutation operator that are used in expressions (2) As reduced program still needs to be working as mostly correct program, we expect simple removals. (3) Minimally reducing expressions, conditions etc further may be very time consuming and hence runtime adaptations may not converge quickly.
\end{comment}
\begin{comment}
Tool:
%\subsubsection{Coverage and dead code removal}
It is important to note that if the program parts that can be reduced only contains non covered entities, just computing coverage and removing non covered entities will do the job. But even if entities are covered, but does not contribute to the correctness of program as defined by test suite, they qualify as reduction candidates. Though \mytool\ can be optimized by allowing non covered entities to be reduced first, something we are planning to do in future. To provide an example, consider following code that was removed as part of our reduction from URLValidator class.

% \begin{lstlisting}[caption={URLValidator reduced code}]
% if (!isValidAuthority(authority)) {
%	return false;
%}
%\end{lstlisting}

The code was reduced when we relaxed specifications by randomly turning off
two tests. The code was reduced though it was covered 42,018 times during
execution. 
\end{comment}
%\subsubsection{Comparison with coverage}
Is a 1-minimal program just a program that contains only the
code covered by the tests in $T'$?   No.  While this may be true for
some extremely thorough test suites, it is not only possible but quite
common for a test to execute code that it does not actually check for
correctness.  Schuler and Zeller propose a notion of \emph{checked
  coverage}~\cite{schuler2011assessing}, which only includes code that
is not only executed, but that has data flow to values checked by some
assertion in a test.  Our notion is similar in that it goes beyond
mere coverage, but even stronger, in that code covered and ``checked''
may be removed from a 1-minimal program.  Consider the following
Python program:

\begin{lstlisting}[caption={Program fragment}, language=python,numbers=left]
def toDiceThrow(val):
   val = val % 6
   return val

def test_toDiceThrow():
   assert(toDiceThrow(1) == 1)
\end{lstlisting}

Here, line $2$ is considered checked (since the value computed flows
to the assertion at line 6), but it can be removed without the test
failing, and so would not appear in a 1-minimal program for this test.

\begin{comment}
\subsection{Selecting subset of test cases}
For object oriented components, components are reduced by reducing the contained
classes, and classes are in turn reduced by reducing the contained methods.
When a class is being reduced, the system needs to be compiled and tests
executed after each unit reduction step. Unfortunately, running all the tests of
a component while reducing a single class can be prohibitive. To speed up the
process, we choose only the most relevant tests for the class under
consideration. For example, while reducing the \emph{Apache commons validator}
project, changing specification of \emph{URLValidator} does not affect the
behavior of \emph{CreditCardValidator} or \emph{CalendarValidator}. Hence, tests
for \emph{CreditCardValidator} or \emph{CalendarValidator} need not be run when
reducing the \emph{URLValidator}. The \mytool allows us to utilize such
information for pre-computing adaptations. That is, one can provide filtering
information at test suite or class level as to which tests should be run. The filtering information can be computed automatically also~\cite{2014kwoncloudrefactoring}.
When runtime adaptations are triggered, to select subset of tests based on feature and resource is an active area of research~\cite{fredericks2013towards}. 


\subsection{Workflow for pre-computed adaptations}
\begin{enumerate}
\item Developer annotates the test suite by labeling the priority of each test case or assertion.
\item For each level of priority, remove all test cases below that priority.
\item Use \mytool to compute the minimal program from the program and remaining test suite.
\end{enumerate} 

\subsection{Workflow to compute adaptations at runtime}

\begin{enumerate}
\item Developer annotates the test suite by labeling the priority of each test case or assertion.
\item Run time adaptations are triggered, where reduction is chosen adaptation.
\item For each level of priority,  remove all test cases below that priority
\item \mytool computes the minimal program from the program and remaining test suite.
\item Check the minimal program against the resource constraint.
\item If resource constraint is not violated keep the reduced program.
\item If it is violated, continue by reducing the priority level by one.
\item If level 0 is reached, output the maximally minimal program.
\end{enumerate}
\end{comment}

\begin{comment}
Alex, should we keep the following section (\mytool under hood?)
We do not have any data showing that our modifications are better
than \HDD. Without that, at this point, I feel that this is more
of a liability. (rahul) 

response from alex:

I think this may be ok, the data showing that the height is never very
'tall' is suporting this.  in fact, what we do is somewhat like trying
all statement deletion mutations before anything else, and then
applying them as we run into them (except we work our way backwards so
we can delete some statements that would have been needed perhaps by
some later thing -- it's a kind of optimization and I think I can
describe why it basically, for our purpose has to be better)  -- this
trying things close to 1-minimality is something MacIver and I have
been talking about and I've seen it work very wel in practice compared
to ``normal'' ddmin appraoches, in python test reduction
}
\end{comment}
\input{tool2}
