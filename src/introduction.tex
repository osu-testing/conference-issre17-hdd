\section{Introduction}

Modern software systems are often complex, and use numerous resources
-- obvious ones, such as
memory, storage, and network bandwidth -- and less obvious
``resources'' such as software libraries.
While developing such software systems, implicit or explicit assumptions are
often made about their resource usage or the availability of resources.
For example, a mobile navigation app assumes availability of
location provider services via network, GPS or other means.
A problem occurs when the software is used in a situation it was not designed
for, where the original resource assumptions made during development no longer hold. For
example, the navigation app may no longer be able to access the network or GPS
satellite when in the middle of a forest or under water.
Moreover, the availability of such resources is often subject to change, with
older resources obsoleted and discarded by newer technology.
These advances in underlying technology can often force software developers to
adapt or evolve their software. For instance, a change in a library that the
software depends on can force its developers to refactor or rewrite part of
the software. These rewrites  often require multiple cycles of development
and testing before the application can be fully adapted, which is
costly, time consuming and error prone.
One way to tackle these problems is to let the software evolve itself in
response to change~\cite{krupitzer2015a}.
Adaptations may manifest as \emph{restricted functionality},
\emph{altered functionality}, or \emph{enhanced functionality}~\cite{hughes2016building}.
% We interacted
% with a team of developers trying to build an adaptive software system.
% We found that developers often design program adaptations through
% \emph{reduction} and \emph{replacements}. In \emph{reduction}, the system
% adapts to depleted or changing resource needs by reducing the application
% functionality. This is often accomplished by ignoring low priority invariants
% while still observing high priority invariants. That is, the application
% continues to provide partial functionality while avoiding the exercise of a few
% features. In \emph{replacement}, applications adapt by swapping a given
% functionality with an equivalent functionality such that the resource
% under stress is used less.

%\subsection{Background}
We are working with a group of developers trying to build a real world resource
adaptive software system for military applications called the \emph{Tactical Situational
Awareness System} (TSA). One of the components of TSA is its location provider.
It continuously sends the location of the device to the server. The location
information typically includes actual location coordinates, images, and streaming
video of the location. The location provider of TSA needs to be available in extreme
locations such as remote battlefields, forests, or under the sea where resource
availability is drastically different from the expected environment. That is,
devices that TSA is deployed on may have varying hardware depending upon the
environment, and parts of available hardware may not function in some environments. Hence, the quantity and quality of resources is not known in
advance and will vary drastically.
Two primary strategies are employed by developers to
make the software adapt to varying resource availability:
%\begin{enumerate}
%\item
{\bf 1) Reduction:} use reduced versions of original components that
do not satisfy all aspects of the original specification, achieved by
removing or avoiding executing part of the code.
%\item 
{\bf 2) Replacement:} use components that are altered from the original
components -- these new components use different libraries, data structures, hardware,
and resources.
%\end{enumerate}

The \emph{reduction} strategy is often used when it is possible to lower
resource usage by relaxing invariants or specifications. It may be accomplished
either by turning off some features or by not executing certain parts of the
code such that a resource under stress will be less utilized, or not utilized at all.
The \emph{replacement} strategy is used when it is possible to come up
with completely different components with different resource profiles.
Usage of these changed components should alter the resource usage so that the
application can weather the resource degradation gracefully.

Currently, both strategies are executed manually. A developer has to
carefully observe component resource needs and consider the
impacts on system specifications
in order to come up with correct adaptations. Further, adapted components
have to be identified, associated with resources, and verified against their resource
consumption. Changes have to be verified against the \emph{new or reduced set of
specifications} they are going to satisfy. All these steps are manual,
error-prone and tedious, and trigger new cycles of development and
testing.  In this paper we focus on a method to allow systems (broadly
considered) to perform \emph{reduction} adaptations themselves, using
only their own test suites.
%We investigate this in the context of resource adaptive software
%systems~\cite{hughes2016building}.

%\subsection{Automatic program reduction using annotated test suites}
Resource adaptive software systems\footnote{Often abbreviated as \RASS.}
are designed and implemented mostly for mission critical software systems.
Hence, they are often accompanied by a high quality test suite that captures and
verifies the specification of the system adequately.
When we perform adaptation by reduction, some of this specification  may have to
be sacrificed. For reduction based adaptations, the
\emph{\sofs} can often be represented cleanly and simply by annotating
the test suite. These annotations may be at the test case level,
at the  invariant/property level, or use a combinations of these
methods. The annotations may take the form of marking test cases that exercise a given
feature or turning off specific asserts or invariant checks.   Given such annotated test suites, we show that an automated program reducer can automatically adapt
a system to use fewer resources.

\begin{comment}
We create reduced variants of the program based on the \sofs captured by an
annotated test suite. The annotations
themselves are simple, and can be inserted either automatically or
manually.
\end{comment}
We perform program reduction using a modified \HDD technique combined with
\emph{statement deletion mutation}. The program is reduced until a
locally minimal version of the program
is found that can still pass all tests corresponding to a certain
level of preserved specification satisfaction.  
%The program adaptation may be triggered automatically at
%runtime in response to a low resource alarm, or selected from a
%database of previously constructed potential adaptations, when
%resource limitations are anticipated.
%Our approach is
%essentially a novel application of \emph{cause reduction}.
Delta-debugging~\cite{zeller2002simplifying} is a binary-search like
algorithm intended to reduce the size of failing test cases.  It
removes components of a test that are not required in order for the
test to fail.  Hierarchical-delta-debugging (HDD)~\cite{misherghi2006hdd} modified the algorithm to produce better
results for tree-structured test inputs, especially, e.g., programs
input to compilers.  \emph{Cause
  reduction}~\cite{stvrcausereduce,icst2014} further
proposed that delta-debugging/HDD are not limited to reducing tests
with respect to the property ``the test fails'' -- they can also
reduce a test so that it is shorter but covers the same code, uses the
same amount of memory, and so forth.  In this paper, we argue that HDD's aim of reducing
inputs that are themselves source code means that
delta-debugging-like methods can also be
used to reduce actual programs, with respect to the property that the
program still passes a set of tests representing required program
behavior.  The basic idea is mentioned in the cause reduction journal
paper~\cite{stvrcausereduce}; our insight is to exploit this concept
for resource adaptation, by only requiring the program to pass some tests,
rather than all tests.  At heart, we are proposing labeling of tests
(and using them to control reduction adaptations) as a response to the
need for a requirements vocabulary that expresses flexibility and
uncertainty proposed in a research roadmaps for software engineering for
self-adaptive systems by Cheng et al.~\cite{cheng2009softwareengineering}.  The
advantage of our approach is that it
is both conceptually
simple and practically applicable to existing systems:  simply by
labeling some tests, a potential reduction can be defined.  Unlike a
novel requirements language, developers
already tend to ``speak'' the
language of tests.
%resource profile.
%\footnote{
%While the program reduction performed may seem similar to
%checked coverage~\cite{schuler2011assessing},
%}.

\subsection{A Simple Example: Removing Logging}

\begin{figure}
\begin{lstlisting}
final static Logger log = Logger.getLogger(logname);
...
   log.info("Begin transmit ",b,"bytes");
...
   if (failed) {
      log.error("Transmit ",b," failed");
      handle_failure();
   } else
      log.info("End transmit ",b,"bytes"); 
\end{lstlisting}

\caption{A fragment of a Java program with potentially over-aggressive logging.}
\label{fig:log}
%\vspace{-0.5in}
\end{figure}


\begin{figure*}
\includegraphics[width=\textwidth]{paper_figure.pdf}
\caption{The test-based approach to resource adaptation by program
  minimization}
\label{fig:workflow}
\end{figure*}

Consider the code in Figure \ref{fig:log}.    Suppose that this is
part of the code for a system that sometimes operates in an embedded
environment with very limited flash file storage, and where core system functionality
requires storing critical image files.  These files require only constant
sized space (they are held until they can be transmitted), but unfortunately the logging information on
transmissions and other events fills the limited flash storage.  While
the logging stops when the space is filled, the system does not have
an automatic way to reclaim the space taken by the logs.  Of course,
the developers could write such a behavior, explicitly, and modern
logging systems are usually relatively easy to configure on the fly.  However, it
is possible to automatically construct a version of the program with
either less logging or no logging, depending upon our needs, that
conforms not to a programmer's possibly erroneous model of which
logging is essential, but to a much more concrete specification of
what the system must log, and how important it is:  which tests fail
when the logging is removed.  Our assumption is that in the long run,
critical software systems require tests that fail when any
important functionality is removed; the novel aspect is that we also
believe it would be highly beneficial to have test suites that, at a
high level, mark \emph{which} functionality each test checks.

Imagine that the test suite for the system in Figure \ref{fig:log} has
250 very thorough tests.  Of these tests, only 3 check the behavior of the
logging of transmission beginning and ending.  An additional 5 tests
check that when a transmission fails, that error is properly logged.
Furthermore, suppose the 3 tests checking begin/end log messages are
labeled {\tt logtransmission} and the 5 tests checking for failure
messages are labeled {\tt logfailtransmission}.    Finally, 50 tests
are labeled {\tt critical}:  if these tests fail the system cannot be
said to provide its most basic, essential functionality.  Other tests
may have other labels (and some tests will have more than one label).  The
most basic version of automatic reduction adaptation proceeds as
follows:  the system selects some set (to start with, of size 1) of labels that does not include
{\tt critical}.  Tests marked with these labels are allowed to fail --
the labels specify \sofs.  We
use an algorithm based on methods for minimizing test cases and
detecting weaknesses in test suites to \emph{automatically generate} a
version of the system that 1) passes all tests that are not marked
with the selected sacrificed labels and 2) is otherwise as small as
possible, within the limits of the algorithm we use to find code to
remove (a locally minimum program).  In this case, if the label selected is {\tt logtransmission},
the reduced version of the system will remove both {\tt info} logging
calls (and the entire {\tt else} block of the {\tt if} statement) but no other code.  If the label is {\tt logfailtransmission} the
code removed will be the {\tt error} logging call.  Finally, if both
labels are sacrificable, all logging calls will be removed and the
creation of the {\tt log} object will also be deleted.  Running the
still-passing tests, a resource monitor can observe that use of
storage was reduced on average (not all tests will have transmissions,
but if some do the change is visible), and add these
removals to a database of adaptations to mitigate storage resource
issues.  Moreover, in addition to storage usage, the adaptation
removing all logging is
also applicable in the event that the system migrates to a platform
with a new, incompatible API for logging (so the old system will not
even build, or will crash when it attempts to access logging).



As discussed below, there are many possible variations on this basic
idea, including on-the-fly generation of variants during system
operation rather than pre-computing reduced variants to apply;
however, the basic concept remains:  determine a set of tests
representing specification aspects that can be sacrificed in pursuit
of resource savings, and compute a version of the system that is reduced
with respect to the constraint that it still passes all other tests.
Figure \ref{fig:workflow} shows the general concept.  Assume that we
are willing to sacrifice the aspects of the specification/functionality represented
by test labels A-D\footnote{Note that we say ``specification/functionality''
for a reason:  while the concepts of specifications and
functionalities in a software system are not identical, tests tend to
conflate specification and functionality, and our approach in many
ways does the same, because its concept of either specification or
functionality is purely test-based.}.  Tests that are not circled have other labels,
and must still pass (and tests 1-9, in bold, are critical tests that
can never be sacrificed).  Our \mytool tool (available at \url{https://github.com/amchristi/hddRASS}) takes this \sofs and computes a
reduced version of the original system that fails most of the tests
labeled A-D (one test still passes, because it actually tests only behavior
required by other labels), and has less code.  The
resources used by the removed code are no longer consumed by the system.

\subsection{Contributions}
The contributions of this paper include: 1) a novel way to capture
\sofs using annotated tests; 2) a reduction tool, \mytool, that can
build an adapted version of a program based on annotated tests; 3) a
case study adapting the NetBeans IDE to use significantly less memory simply by
labeling 3 tests corresponding to the \emph{undo-redo} functionality
and applying \mytool and 4) an empirical evaluation of reduction
achieved using randomly annotated test suites for real-world open
source programs.

\begin{comment}
\begin{itemize}
\item A novel way to capture \sofs using test case annotations.
\item A reduction tool, \mytool\footnote{
Available for download at \url{https://github.com/amchristi/hddRASS}.
}, that can build an adapted version of a program using an annotated test suite.
\item A case study adapting NetBeans IDE for lower resource usage by applying
\mytool. This is accomplished by annotating just three test cases corresponding
to the \emph{undo-redo} feature. Sacrificing this feature reduced the
IDE's memory consumption significantly.
\item An empirical evaluation of reductions achieved using randomly annotated
test suites for large real world open source programs.
\end{itemize}
\end{comment}